from urllib.parse import quote_plus
import argparse
import os

_README_="""return URL
"""

_SERVER_='http://localhost:5000'

def run(inFile):
    return os.path.join(_SERVER_, 'run', quote_plus(os.path.abspath(inFile)[1:]))

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=_README_
    )
    parser.add_argument('-i', metavar='i', required=True, help='in file')
    
    args = parser.parse_args()
    
    res = run(args.i)
    
    print(res)
    

if __name__ == '__main__':
    main()
