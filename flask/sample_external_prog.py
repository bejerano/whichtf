import argparse

_README_="""Sample external program.
"""

def run(inFile):
    return '{} ({})'.format(inFile, len(inFile))

def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=_README_
    )
    parser.add_argument('-i', metavar='i', required=True, help='in file')
    
    args = parser.parse_args()
    
    res = run(args.i)
    
    print(res)
    

if __name__ == '__main__':
    main()
