#!/bin/bash

assembly=hg19
assemblyPath=/cluster/data/ontologies/hg19/20171029/hg19.loci
ontologyName=MGIPhenotype
ontologyPath=/cluster/data/ontologies/hg19/20171029/MGIPhenotype/20171029/ontoToGene.canon

cd ..
bin/WhichTF buildOnt $assembly $assemblyPath $ontologyName $ontologyPath -v
cd demos

