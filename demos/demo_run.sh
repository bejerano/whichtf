#!/bin/bash
set -beEuo pipefail

assembly=hg19
inFile=data/demos/ENCFF719GOE.bed

# run WhichTF with --outFile --partialScore, and --outPartialScore flags
(
    set -x
    WhichTF "${inFile}" "${assembly}" \
        --outFile ${inFile%.bed}.whichtf.tsv \
        --partialScore \
        --outPartialScore ${inFile%.bed}.whichtf.terms.tsv
)

# show the contents of the results file
cat ${inFile%.bed}.whichtf.tsv
