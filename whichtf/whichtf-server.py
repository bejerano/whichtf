from __future__ import division
import subprocess
import os,csv,collections,tempfile,itertools
import numpy as np
import pandas as pd
from scipy import sparse
from scipy import stats
from logging import getLogger

import rpy2.robjects as robjects
from rpy2.robjects import numpy2ri
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage

from flask import Flask

from .run import load_assembly, load_ontology, Run_task_server
from .configs import parse_argv

app = Flask(__name__)

class WhichTF_reference:
    '''
    This class provides instances that store reference data for a specific
    combination of assembly, tfbs, and ontology.
    '''
    def __init__(self, data, assembly, tfbs, ont):
        self.assembly=assembly
        self.tfbs=tfbs
        self.ont=ont

        self.reg_dict, self.tf_dict, self.tf_sparse_data = load_assembly(
            os.path.join(data, self.assembly, self.tfbs, 'tfbs.data.npz')
        )
        self.term_dict, self.term_sparse_data = load_ontology(
            os.path.join(data, self.assembly, self.tfbs, 'ontologies', ont, 'ont.data.npz')
        )
        print(assembly, tfbs, ont)


def main():
    data = os.path.join('/webapp', 'whichtf', 'data')
    assembly_names=['hg19']
    tfbs_names=['PRISM']
    ont_names=['MGIPhenotype']

    whichtf_ref = dict([])
    for assembly, tfbs, ont in itertools.product(assembly_names, tfbs_names, ont_names):
        whichtf_ref[(assembly, tfbs, ont)] = WhichTF_reference(data, assembly, tfbs, ont)

    return whichtf_ref


whichtf_ref = main()
great_results_root=os.path.join('/scratch', 'great', 'tmp', 'results')
WhichTF_ref_dir='/webapp/whichtf/data'
overlapSelect='/webapp/whichtf/bin/overlapSelect'


@app.route('/')
def hello():
    return "Hello World!"

@app.route("/<assembly>/<tfbs>/<ont>/<sessionName>")
def results(assembly, tfbs, ont, sessionName):
    '''$curl http://localhost:5000/hg19/PRISM/MGIPhenotype/20180807-public-4.0.3-hYc14B'''

    # 'data/demo/ENCFF719GOE.bed', 'hg19', '-v', '--outFile', 'data/demo/ENCFF719GOE.whichtf.tsv', '--data', '/webapp/whichtf/data', '--overlapSelect', 'bin/overlapSelect'
    sessionDir=os.path.join(great_results_root, '{}.d'.format(sessionName))
    inFile=os.path.join(sessionDir, 'fore.bed')
    outFile=os.path.join(sessionDir, 'whichtf.tsv')

    _, config = parse_argv(
        [inFile, assembly, '-v', '--outFile', outFile,
        '--data', WhichTF_ref_dir, '--overlapSelect', overlapSelect]
    )

    for key, val in config.items_all():
        print('{}: {}'.format(key, val))
    print('')

    ref=whichtf_ref[(assembly, tfbs, ont)]

    run_task = Run_task_server(
        config,
        ref.reg_dict, ref.tf_dict, ref.tf_sparse_data,
        ref.term_dict, ref.term_sparse_data
    )
    results = run_task.run()
    results.to_csv(config.get('outFile'), index=False, sep='\t')
    # write to JSON here.

    return '{} {} {} {}'.format(assembly, tfbs, ont, sessionName)
