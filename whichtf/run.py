from __future__ import division
import os
#import sys
import subprocess
import csv
import numpy as np
from scipy import sparse
from scipy import stats
import pandas as pd
from logging import getLogger
import collections
import tempfile

import rpy2.robjects as robjects
from rpy2.robjects import numpy2ri
from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage

np.seterr(all='warn')
np.seterr(under='warn')

logger = getLogger("run.py")

##############################
# R functions wrapper #
##############################

R_func_wrapper = SignatureTranslatedAnonymousPackage(
    """
    hyper_logsf <- function(q, m, n, k) {
        return(-phyper(q = q, m = m, n = n, k = k, log.p=TRUE, lower.tail = FALSE)/log(10))
    }
    binom_logsf <- function(q, size, prob) {
        return(-pbinom(q = q, size = size, prob = prob, log.p=TRUE, lower.tail = FALSE)/log(10))
    }
    """,
    "R_func_wrapper"
)


#
# Note, the work flow here is that we OS query with merged tfbs terms to
# generate terms. Not same as OS then merge. Also not same as no merge.
# we should make sure this gives a reasonable distribution of lengths and is
# what we actually want.

def gen_regions(query_path, tfbs_path, region_out_path, settings):
    """
    This function selects the tfbs regions overlapping the query. It is a long
    winded form of:

    cut -f1-3 query_path
        | overlapSelect -selectFmt=bed stdin tfbs_path region_out_path

    Inputs:
        query_path - Path to query bed file.
        tfbs_path - Path to merged bed file containing all tfbs.
        region_out_path - Path to write list of tfbs overlaping query.
        settings - Dictionary containing settings.

    Outputs:
        Writes a bed file containing tfbs overlaping query to disk.
    """

    logger.debug('gen_regions()')
    buz = subprocess.Popen(('cut', '-f1-3',query_path), stdout=subprocess.PIPE)
    res = subprocess.call((settings['overlapSelect'], '-selectFmt=bed', 'stdin',
                           tfbs_path, region_out_path), stdin=buz.stdout)
    buz.wait()
    logger.debug('return value = {}'.format(res))
    return res


def gen_terms(region_out_path, settings, tmp_dir=None):
    """
    This function merges the tfbs regions and uses them to generate a list of
    terms. The mesthod is specified by settings['termMethod']. It then selects
    the top 100 terms.

    Inputs:
        region_out_path - Path to read list of tfbs overlaping query.
        settings - Dictionary containing settings.
        tmp_dir - Directory to write temporary files.

    Outputs:
        top_terms - List of most enriched terms satifying cuts.
    """
    termMethod = settings.get('termMethod')
    logger.debug(termMethod)

    if termMethod == 'GREATER':
        top_terms = greater_terms(region_out_path, settings)

    elif termMethod == 'bin':
        top_terms = bin_terms(region_out_path, settings)

    elif termMethod == 'hyper':
        # To Do: Write Hyper
        logger.warning('hyper not written yet')

    elif termMethod == 'file':
        top_terms = file_terms(settings)

    else:
        logger.warning('Invalid term method.')
        quit()

    return top_terms

#################################################
# Defs associated with GREATER run mode #########
#################################################

def greater_terms(region_out_path, settings, tmp_dir=None):
    """
    This function merges the tfbs regions and pushes them through GREATER.
    It then selects the top termNum terms satisfying min and max annotation num.

    Inputs:
        region_out_path - Path to read list of tfbs overlaping query.
        settings - Dictionary containing settings.
        tmp_dir - Directory to write temporary files.

    Outputs:
        top_terms - List of most enriched terms satifying cuts.
    """
    if tmp_dir == None:
        tmp_dir = '/'.join(region_out_path.split('/')[:-1])+'/tmp'

    if not os.path.exists(tmp_dir):
        os.makedirs(tmp_dir)

    GREATER_out_path = '_'.join([
        str(x) for x in
        ['GREATER', settings['assembly'], settings['myOnt'],
         settings['myMin'], settings['myMax']]
    ])

    GREATER_out_path = os.path.join(tmp_dir, '{}.out'.format(GREATER_out_path))

    # Compute GREAT results from merged tracks.
    subprocess.call(
        (settings['GREATER'], '--noHeader',
         '--ontologies={}'.format(settings['myOnt']),
         '--maxAnnotCount={}'.format(settings['myMax']),
         '--minAnnotCount={}'.format(settings['myMin']),
         settings['assembly'],
         region_out_path,
         GREATER_out_path)
    )

    GREATER_results_path = os.path.join(GREATER_out_path, '{}.tsv'.format(settings['myOnt']))

    # Turn GREAT results into a list of top terms.
    top_terms = [None] * int(settings['termNum'])
    term_idx = 0
    with open(GREATER_results_path, 'r') as f:
        for line in f:
            if(line[0] == '#'):
                pass
            top_terms[term_idx] = line.split('\t')[0]
            term_idx += 1
            if(term_idx >= int(settings['termNum'])):
                break
    top_terms = [x for x in top_terms if x is not None]
    logger.debug('{} terms are selected (specified option : {})'.format(len(top_terms), settings['termNum']))

    # Remove temporary files.
    if not settings['leaveTrace']:
        GREATER_files = os.listdir(GREATER_out_path)
        for res_file in GREATER_files:
            os.remove(os.path.join(GREATER_out_path, res_file))
        os.rmdir(GREATER_out_path)
        try:
            os.rmdir(tmp_dir)
        except OSError:
            print('Did not remove %s as it was non-empty.') % tmp_dir

    return top_terms

#################################################
# Defs associated with bin run mode #############
#################################################
#@profile
def bin_terms(region_out_path, settings):
    """
    Rank terms by binomial p-value and take top termNum of terms.
    AKA, poor_man's_great.

    Inputs:
        region_out_path - Path to input query region intersect tfbs.
        settings - Dictionary of settings. In particular:
            settings['termNum'] - Number of terms to keep.
            settings['myMax'] - Max number of genes for term.
            settings['myMin'] - Min number of genes for term.

    Outputs:
        top_terms - List of most enriched terms satifying cuts.

    """
    term_cut = int(settings['termNum'])
    min_genes = int(settings['myMin'])
    max_genes = int(settings['myMax'])

    ont_query_file =  os.path.join(settings['data'],
                                   settings['assembly'], settings['tfbs'],
                                   'ontologies', settings['myOnt'],
                                   'ont.merge.query.npz')

    term_sizes = os.path.join(settings['data'],
                              settings['assembly'], 'ontologies',
                              settings['myOnt'], 'regdoms.sizes.tsv')
    chrom_sizes = os.path.join(settings['data'],
                               settings['assembly'], 'chrom.sizes')

    reg_dict, term_dict, query_sparse_data = load_assembly(ont_query_file)

    # Get selected regions and qualifying terms
    run_regions = hash_regions(region_out_path, reg_dict)
    cut_terms = sub_terms(min_genes, max_genes, settings)
    cut_terms = [term for term in cut_terms if term in term_dict]

    cut_term_ids = [term_dict[term] for term in cut_terms]

    # Subset sparse data to regions and terms
    hits = query_sparse_data[run_regions,:][:,cut_term_ids]

    # Compute binomial enrichment over terms
    hits = np.array(hits.sum(axis=0)).reshape(len(cut_terms))
    N = len(run_regions)
    prob_vec = p_vec(cut_term_ids, term_sizes, term_dict,
                     genome_size=chrom_sizes)

    numpy2ri.activate()
    log_bpvals = np.array([
        R_func_wrapper.binom_logsf(
            q = hits[i] - 1,
            size = N,
            prob = prob_vec[i],
            )[0]
            for i in range(len(hits))])
    numpy2ri.deactivate()

    # Take top
    rev_terms = {i: cut_terms[i] for i in range(len(cut_terms))}
    top_terms = [rev_terms[term] for term in np.argsort(-log_bpvals)[:term_cut]]

    return top_terms

def sub_terms(min_genes, max_genes, settings):

    processed_ont_dir = os.path.join(settings['data'],
                                     settings['assembly'], 'ontologies',
                                     settings['myOnt'])

    g_counts = os.path.join(processed_ont_dir, 'gene.counts.tsv')

    good_terms = []

    with open(g_counts, 'r') as terms:
        myReader = csv.reader(terms, delimiter='\t')
        for row in myReader:
            num = int(row[1])
            if (min_genes <= num) and (num <= max_genes):
                good_terms.append(row[0])

    return good_terms

def file_terms(settings):
    """
    This function reads in term ids from file.

    Inputs:
        settings - Dictionary containing settings.

    Outputs:
        top_terms - A list of top terms.
    """

    term_file = settings['termFile']

    top_terms = []
    with open(term_file, 'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')
        for row in myReader:
            term = row[0]
            top_terms.append(term)

    return top_terms


def load_assembly(path):
    """
    This function loads the processed assemply data to a compressed file.

    Inputs:
        path - Path to store data.

    Outputs:
        reg_dict - Dictionary mapping PRISM regions to keys.
        tf_dict - Dictionary mapping TFs to keys.
        tf_sparse_data - Sparse region X TF matrix.
    """
    print(path)

    logger.debug('load_assembly({})'.format(path))
    loaded = np.load(path, allow_pickle=True)

    reg_dict=loaded['reg_dict'].item()
    tf_dict=loaded['tf_dict'].item()
    tf_data=loaded['tf_data']
    tf_indices=loaded['tf_indices']
    tf_indptr=loaded['tf_indptr']
    tf_shape=tuple(loaded['tf_shape'])

    tf_sparse_data = sparse.csr_matrix((tf_data, tf_indices, tf_indptr),
                                       shape=tf_shape)

    return reg_dict, tf_dict, tf_sparse_data

def load_ontology(path):
    """
    This function loads the processed ontology data to a compressed file.

    Inputs:
        path - Path to store data.

    Outputs:
        term_dict - Dictionary mapping ontology terms to keys.
        term_sparse_data - Sparse region X term matrix.
    """

    loaded = np.load(path, allow_pickle=True)

    term_dict=loaded['term_dict'].item()
    term_data=loaded['term_data']
    term_indices=loaded['term_indices']
    term_indptr=loaded['term_indptr']
    term_shape=tuple(loaded['term_shape'])

    term_sparse_data = sparse.csr_matrix((term_data, term_indices, term_indptr),
                                         shape=term_shape)

    return term_dict, term_sparse_data

def hash_regions(run_region_file, reg_dict):
    """
    This function takes in the tfbs overlaping the query and computes a sorted
    hashed list.

    Inputs:
        run_region_file - The file containing the tfbs overlapping the submitted
                          bed file.
        reg_dict - The dictionary hashing the tfbs.

    Outputs:
        run_regions - Sorted hashed list of tfbs overlaping query.
    """
    run_regions = []

    with open(run_region_file, 'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')
        for row in myReader:
            myReg = '_'.join(row[0:3])
            run_regions.append(reg_dict[myReg])

    run_regions.sort()

    return run_regions

def hash_terms(run_term_file, term_dict):
    """
    This function takes in a list of enriched terms from the query and computes
    a hashed list.

    Inputs:
        run_term_file - The file containing the list of terms.
        term_dict - The dictionary hashing the terms.

    Outputs:
        run_terms - Hashed list of terms enriched in query.
    """
    run_terms = []

    with open(run_term_file, 'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')
        for row in myReader:
            myTerm = row[0]
            run_terms.append(term_dict[myTerm])

    return run_terms

########################################
########################################
##                                    ##
##              Features              ##
##                                    ##
########################################
########################################

def run_matrix(run_regions, run_terms, tf_sparse_data, term_sparse_data):
    """
    This function takes in hashed region and term data from a given run to
    compute a term X TF hit matrix.

    Inputs:
        run_regions - Sorted hashed list of tfbs overlaping query.
        run_terms - Hashed list of terms enriched in query.
        tf_sparse_data - The sparse region X TF matrix.
        term_sparse_data - The sparse region X term matrix.

    Outputs:
        run_term_tf - A term X TF numpy array containing the number of hits.
    """
    run_tf_mat = tf_sparse_data[run_regions,:]
    run_term_mat = term_sparse_data[run_regions,:][:,run_terms]

    run_term_tf = run_term_mat.T.dot(run_tf_mat).toarray()

    n_vec = tf_sparse_data[run_regions,:].sum(axis=0)

    return run_term_tf, n_vec


def debug_dump_p_val_matrix(p_val_matrix, logscale = True):
    """
    Debug function. Investigate p_val_matrix and dump the shape, min, max, and
    number of values < machine_epsilon
    """
    logger.debug('p_val_matrix shape = {}, min = {}, max = {}'.format(
        p_val_matrix.shape, np.min(p_val_matrix), np.max(p_val_matrix)
    ))

    if(logscale):
        logger.debug('smaller than machine epsilon: total {}, row {} , column {}'.format(
            np.sum(p_val_matrix > -np.log10(np.finfo(float).eps)),
            collections.Counter(np.sum(p_val_matrix > -np.log10(np.finfo(float).eps), axis = 1)),
            collections.Counter(np.sum(p_val_matrix > -np.log10(np.finfo(float).eps), axis = 0))
        ))

    else:
        logger.debug('smaller than machine epsilon: total {}, row {} , column {}'.format(
            np.sum(p_val_matrix < np.finfo(float).eps),
            collections.Counter(np.sum(p_val_matrix < np.finfo(float).eps, axis = 1)),
            collections.Counter(np.sum(p_val_matrix < np.finfo(float).eps, axis = 0))
        ))

#@profile
def hyper_mat(run_term_tf, n_vec):
    """
    Compute -log10(hypergeometric p-values) for each term X TF combination.

    Inputs:
        tf_sparse_data - Sparse region X TF matrix.
        run_term_tf - A term X TF numpy array containing the number of hits.

    Outputs:
        p_val_mat - Matrix of -log10(p_values) for each term and TF.
    """
    logger.debug('hyper_mat() start')

    N_val = n_vec.sum()
    K_vec_1d = run_term_tf.sum(axis=1)

    numpy2ri.activate()
    p_val_matrix = np.array([[
        R_func_wrapper.hyper_logsf(
            q = run_term_tf[i, j] - 1,
            m = K_vec_1d[i],
            n = N_val - K_vec_1d[i],
            k = n_vec[0, j]
            )[0]
            for j in range(run_term_tf.shape[1])]
            for i in range(run_term_tf.shape[0])])
    numpy2ri.deactivate()

    debug_dump_p_val_matrix(p_val_matrix, logscale = True)

    logger.debug('hyper_mat() end')

    return p_val_matrix


def hyper_sub_mat(tf_sparse_data, run_term_tf):
    """
    Compute hypergeometric p-values for each term X TF combination.

    Inputs:
        tf_sparse_data - Sparse region X TF matrix.
        run_term_tf - A term X TF numpy array containing the number of hits.

    Outputs:
        p_val_mat_sbNull - Matrix of p_values, null computed with instance
        subtracted.
    """
    from scipy.stats import hypergeom

    n_vec = tf_sparse_data.sum(axis=0)
    N_val = n_vec.sum()
    N_vec = N_val-n_vec
    K_vec = run_term_tf.sum(axis=1).reshape(run_term_tf.shape[0],1)
    K_mat = K_vec-run_term_tf

    # TO DO:
    #   Need to implement control for when a single TF is giving all signal
    #   p_val_matrix_sbNull should not be trusted until this is done!!!!


    p_val_matrix_sbNull = hypergeom.sf(run_term_tf,
                                       N_vec,
                                       K_mat,
                                       n_vec,
                                       loc=0)

    return p_val_matrix_sbNull


# Note:
#
# This binomial feature is modeled after what GREAT does. In this sense, it does
# not take advantage of the PRISM data set as a null at all. This would be a
# natural thing to investigate changing.
#

def p_vec(run_terms, term_sizes, term_dict, genome_size=None):
    """
    Compute vector of probabilities for each term.

    Inputs:
        run_terms - set of terms to retrive probs for.
        term_sizes - tsv containing term reg domain size.
        term_dict - term hash dict.
        genome_size - Assembly size, or chrom.sizes path, or default is to
                      compute from the total term_sizes, note this is by not at
                      all gaurenteed to be a good proxy (fore example, term reg
                      doms are not independent).

    Outputs:
        prob_vec - Vector of probabilities for each term in run_terms.

    """
    if genome_size == None:
        genome_size = 0

    elif type(genome_size) == str:
        with open(genome_size, 'r') as myFile:
            myReader = csv.reader(myFile, delimiter='\t')

            genome_size = 0

            for row in myReader:
                genome_size += int(row[1])


    size_dict = {}
    with open(term_sizes, 'r') as myFile:
        myReader = csv.reader(myFile, delimiter='\t')

        for row in myReader:
            if row[0] in term_dict:
                size_dict[term_dict[row[0]]] = int(row[1])
            if genome_size == None:
                genome_size += int(row[1])
                print('Updated genome size: %f' % genome_size)
                
    prob_vec = np.zeros((len(run_terms)))

    for i in range(len(run_terms)):
        prob_vec[i] = size_dict[run_terms[i]]/float(genome_size)

    return prob_vec

#@profile
def binom_mat(run_term_tf, n_vec, prob_vec):
    """
    Compute binomial p-values for each term X TF combination.

    Inputs:
        run_term_tf - A term X TF numpy array containing the number of hits.
        n_vec - Vector of total query number
        prob_vec - Vector given fraction of genome taken by each term.

    Outputs:
        p_val_mat - Matrix of -log10(p_values) for each term and TF.
    """
    logger.debug('binom_mat() start')

    numpy2ri.activate()
    p_val_matrix = np.array([[
        R_func_wrapper.binom_logsf(
            q = run_term_tf[i, j] - 1,
            size = n_vec[0, j],
            prob = prob_vec[i],
            )[0]
            for j in range(run_term_tf.shape[1])]
            for i in range(run_term_tf.shape[0])])
    numpy2ri.deactivate()

    debug_dump_p_val_matrix(p_val_matrix, logscale = True)

    logger.debug('binom_mat() end')

    return p_val_matrix


def first_n_filter(hyp_p, bin_p, first_n, first_n_strict, is_p_value_in_log_scale):
    '''
    Demand that the first n most significant TFs are concordant.
    Either identical or drawn from the same set.
    '''
    assert(first_n > 0)
    assert(hyp_p.shape == bin_p.shape)

    p_value_sort_direction = -1 if is_p_value_in_log_scale else 1
    logger.debug('first_n_filer() p_value_sort_direction = {}'.format(p_value_sort_direction))

    hyp_p_top_n_TFs = np.argsort(p_value_sort_direction * hyp_p, axis=1)[:, :first_n]
    bin_p_top_n_TFs = np.argsort(p_value_sort_direction * bin_p, axis=1)[:, :first_n]

    num_terms = hyp_p_top_n_TFs.shape[0]

    if(first_n == 1 or first_n_strict):
        return np.all(hyp_p_top_n_TFs == bin_p_top_n_TFs, axis = 1).reshape(num_terms, 1)
    else:
        return np.array(
            [set(hyp_p_top_n_TFs[i]) == set(bin_p_top_n_TFs[i])
             for i in range(num_terms)]
        ).reshape(num_terms, 1)


def jump_score(p_val_matrix, max_tf=10, is_p_value_in_log_scale=True):
    """
    Compute the fold jump of the min(#tfs in mat, max_tf) and record who is
    before jump.

    Inputs:
        p_val_matrix - Term X TF matrix of p values.
        max_tf - Maximum number of TFs to use for calculating jump.
        is_p_value_in_log_scale - whether p_val_matrix is -log_10(p-value) or not

    Outputs:
        before_jump - Term X TF boolean matrix of whether TF is before jump for
                      term.
        fold_jump - Term X TF matrix of how many fold the jump is of the mean.
    """
    assert(len(p_val_matrix.shape) == 2)

    num_terms = p_val_matrix.shape[0]
    max_tf = min(p_val_matrix.shape[1], max_tf)

    if is_p_value_in_log_scale:
        log_p_val_matrix = p_val_matrix
    else:
        log_p_val_matrix = -np.log10(p_val_matrix)

    # sort p-values
    log_p_val_argsort = np.argsort(-log_p_val_matrix, axis=1)

    # slice top (max_tf) TFs
    log_p_val_top = np.array([
        log_p_val_matrix[i, log_p_val_argsort[i, :max_tf]]
        for i in range(num_terms)
    ])

    # compute diff(-log10(p-value)) for the top (max_tf) TFs
    diffs=np.diff(-log_p_val_top)
    # identify jump position
    diff_jump_position = 1 + np.argmax(diffs, axis=1)[:, None]
#    diff_jump_position = np.argmax(diffs, axis=1)[:, None]
    # compute fold jump
    diff_jump_fold = diffs / np.mean(diffs, axis=1)[:, None]

    log_p_val_cut = np.array([
        log_p_val_top[i, diff_jump_position[i]]
        for i in range(num_terms)
    ])

    logger.debug(
        'log_p_val_cut shape = {}, min = {}, max = {}'.format(
            log_p_val_cut.shape, np.min(log_p_val_cut), np.max(log_p_val_cut)
        )
    )

    before_jump = log_p_val_matrix >= log_p_val_cut

    fold_jump = np.zeros_like(log_p_val_matrix, dtype=np.float)
    for i in range(num_terms):
        fold_jump[i, log_p_val_argsort[i, :max_tf-1]] = diff_jump_fold[i, :]

    return before_jump, fold_jump


def compute_filter(settings, is_p_value_in_log_scale=True, **kwargs):
    """
    This function computes a mask corresponding to the various filtering
    requirements passed.

    Inputs:
        **kwargs - This is keyworded list containing one or both of the hyper
                   and binomial p-values: hyp_p=MATRIX, bin_p=MATRIX.
        settings - This is the dictionary of settings.
            In particular it contains the filters to apply:
                settings['equal_first_n'] - This many most significant TFs must
                                            match.
                settings['first_n_strict'] - Ordered strictly, or same set
                settings['max_tf'] - Number of TFs to use in measuring jumps.
                settings['before_hyp_jump'] - Only TFs below a hyper p-value
                                              jump.
                settings['hyp_jump_thresh'] - The relative jump must be at least
                                              this big.
                settings['before_bin_jump'] - Only TFs below a binomial p-value
                                              jump.
                settings['bin_jump_thresh'] - The relative jump must be at least
                                              this big.

    Outputs:
        mask - This is the filter mask on the data.
    """
    have_hyp = 'hyp_p' in kwargs
    have_bin = 'bin_p' in kwargs

    mask = 1

    if have_hyp:
        hyp_p = kwargs['hyp_p']
        mask = np.ones_like(hyp_p).astype(bool)

    if have_bin:
        bin_p = kwargs['bin_p']
        mask = np.ones_like(bin_p).astype(bool)

    first_n = settings['equal_first_n']


    if first_n > 0:
        logger.info('Applying equal first n filter.')
        try:
            assert have_hyp and have_bin
            mask *= first_n_filter(
                hyp_p, bin_p,
                first_n=first_n, first_n_strict=settings['first_n_strict'],
                is_p_value_in_log_scale=is_p_value_in_log_scale,
            )
        except AssertionError as e:
            logger.warning(e)
            logger.warning(
                'Need to specify both the hyper p-value and binomial p-value'
                ' matrices.'
            )

    # Demand that the tfs fall before a jump in hyper significance that meets
    # the threshold.
    if settings['before_hyp_jump']:
        logger.info('Applying before hyper jump filter.')
        try:
            assert have_hyp

            before_jump, fold_jump = jump_score(
                hyp_p, max_tf=settings['max_tf'],
                is_p_value_in_log_scale=is_p_value_in_log_scale,
            )

            # Before jump
            mask *= before_jump

            # Cutoff on jump size
            column = np.max(fold_jump, axis=1) > settings['hyp_jump_thresh']
            mask *= column.reshape((len(column),1))

        except AssertionError as e:
            logger.warning(e)
            logger.warning('Need to specify the hyper p-value matrix.')

    # Demand that the tfs fall before a jump in binomial significance that meets
    # the threshold.
    if settings['before_bin_jump']:
        logger.info('Applying before binomial jump filter.')
        try:
            assert have_bin

            before_jump, fold_jump = jump_score(
                bin_p, max_tf=settings['max_tf'],
                is_p_value_in_log_scale=is_p_value_in_log_scale,
            )

            # Before jump
            mask *= before_jump

            # Cutoff on jump size
            column = np.max(fold_jump, axis=1) > settings['bin_jump_thresh']
            mask *= column.reshape((len(column),1))

        except AssertionError as e:
            logger.warning(e)
            logger.warning('Need to specify the binomial p-value matrix.')

    return mask


def compute_score(hyp_p_val_matrix, mask=1, is_p_value_in_log_scale=True):
    """
    Compute the score over TFs.

    Inputs:
        rank_matrix - Matrix of ranks for each TF for each term.
        mask - Filter mask to use.
        is_p_value_in_log_scale - Whether the hyper p_value matrix is -log10(p-value)-transformed or not.

    Outputs:
        score - List of scores for each TF.
    """

    sort_direction = -1 if is_p_value_in_log_scale else 1
    logger.debug('compute_score(). sort_direction is {}'.format(sort_direction))
    # For each term, compute ranks of TFs. If there is a tie, the average of the rank will be used.

    my_ranks = np.array([
        stats.rankdata(sort_direction * hyp_p_val_matrix[i])
        for i in range(hyp_p_val_matrix.shape[0])
    ])

    multiplier = np.array([(i+1) for i in range(my_ranks.shape[0])])
    multiplier = multiplier.reshape(my_ranks.shape[0],1)

    my_ranks *= multiplier
    score_mat = mask*(1/np.sqrt(my_ranks))
    score = np.sum(score_mat, axis=0)

    return score_mat, score


def generate_results_df(score, tf_list, pv, settings):
    """
    """
    data = {'TF': tf_list, 'Score': score, 'Pvalue': pv}

    results = pd.DataFrame(
        data=data
    ).sort_values(
        by='Score', ascending=False
    )
    results['Rank'] = 1 + np.arange(len(results))

    rank_cut  = results['Rank']  < settings['max_rank']
    score_cut = results['Score'] > settings['min_score']

    return results[rank_cut & score_cut][['Rank', 'TF', 'Score', 'Pvalue']]

def generate_term_contribution_df(score_mat, tf_list, term_names):
    """
    Given a (filtered) score matrix, we tabulate contribution of 
    each term to the TF ranking

    Input:
      - score_mat: score matrix of size #(terms) x #(TFs)
      - tf_list: column labels
      - term_names: row labels

    Output:
      - a pandas data frame that has the following columns:
        - TF: Transcription factor
        - term: ontology term
        - WhichTF_term_partial_score: contribution of each term for the final score
        - WhichTF_score: the WhichTF score (sum of the term partial score across terms)
    """    
    df = pd.DataFrame(score_mat, index=term_names, columns=tf_list)        
    df['term'] = term_names
    melted = pd.melt(df, id_vars=['term'], var_name='TF', value_name='WhichTF_term_partial_score')    
    res_df = melted[ melted['WhichTF_term_partial_score']!= 0 ].merge(
        pd.DataFrame({
            'WhichTF_score': np.sum(score_mat, axis=0),
            'TF': tf_list
        }), on='TF'
    ).sort_values(
        by=['WhichTF_score', 'WhichTF_term_partial_score'], ascending=False
    )[['TF', 'term', 'WhichTF_term_partial_score', 'WhichTF_score']]
    return(res_df)



##############################################
# Current implementation of GREAT phylosophy #
##############################################

def pmg(region_out_path, assembly, settings):
    """
    pmg (poor-man's-great): Mimics partial output of GREAT.

    Inputs:
        region_out_path - Path to input query region intersect tfbs.
        assembly - Name of assembly to query.

    Outputs:
        results - Pandas dataframe containing output.

    """

    ont_query_file = os.path.join(settings['data'],
                                  settings['assembly'], settings['tfbs'],
                                  'ontologies', settings['myOnt'],
                                  'ont.merge.query.npz')

    term_sizes = os.path.join(settings['data'],
                              settings['assembly'], 'ontologies',
                              settings['myOnt'], 'regdoms.sizes.tsv')

    chrom_sizes = os.path.join(settings['data'],
                               settings['assembly'], 'chrom.sizes')

    processed_ont_dir = os.path.join(settings['data'],
                                     settings['assembly'], 'ontologies',
                                     settings['myOnt'])

    g_count_file = os.path.join(processed_ont_dir, 'gene.counts.tsv')

    reg_dict, term_dict, query_sparse_data = load_assembly(ont_query_file)
    rev_terms = {term_dict[key]: key for key in term_dict}

    run_regions = hash_regions(region_out_path, reg_dict)
    run_term_ids = term_dict.values()
    run_term_ids.sort()

    run_term_names = [rev_terms[key] for key in run_term_ids]

    # Subset sparse data to regions and terms
    hits = query_sparse_data[run_regions,:]

    # Compute binomial enrichment over terms
    hits = np.array(hits.sum(axis=0)).ravel()
    N = len(run_regions)
    prob_vec = p_vec(run_term_ids, term_sizes, term_dict,
                         genome_size=chrom_sizes)
    tots = np.zeros_like(hits)
    tots[:] = N

    numpy2ri.activate()
    bin_p = np.array([
        R_func_wrapper.binom_logsf(
            q = hits[i] - 1,
            size = N,
            prob = prob_vec[i],
            )[0]
            for i in range(len(hits))])
    numpy2ri.deactivate()

    g_counts_dict = {}

    with open(g_count_file, 'r') as terms:
        myReader = csv.reader(terms, delimiter='\t')
        for row in myReader:
            num = int(row[1])
            g_counts_dict[row[0]] = num

    gene_counts = [g_counts_dict[key] for key in run_term_names]

    data = {'bin_p': bin_p,
            'k': hits,
            'n': tots,
            'bin_prob': prob_vec,
            'term_id': run_term_ids,
            'term_name': run_term_names,
            'gene_count': gene_counts}

    results = pd.DataFrame(data = data)
    results.sort_values(by='bin_p', ascending=False, inplace=True)

    results['bin_rank'] = list(range(len(bin_p)))

    return results

############################################
#                                          #
# Defs associated with score p value       #
#                                          #
############################################
#@profile
def compute_lnf_hash(int_mat, bins=1000):

    bc = list(map(np.bincount, int_mat))
    count_list = list(map(lambda x: x[x>0], bc))
    unique_list = list(map(lambda x: np.array(np.nonzero(x)), bc))

    lnf_hash = {0: 0}

    for i in range(len(int_mat)):

        jumps = (to_array(lnf_hash.keys()).reshape(1,-1) + unique_list[i].reshape(-1,1)).ravel()
        lcounts = (to_array(lnf_hash.values()).reshape(1,-1) + np.log10(count_list[i].reshape(-1,1))).ravel()

        lnf_hash = {}

        jump_len = len(jumps)

        for j in range(jump_len):
            key = jumps[j]
            if key in lnf_hash:
                lnf_hash[key] += np.log10(1 + np.power(10, lcounts[j] - lnf_hash[key]))
            else:
                lnf_hash[key] = lcounts[j]
    return lnf_hash

def log10_sum(lnf_list):
    lmax = max(lnf_list)

    lsum = lmax + np.log10(np.power(10,lnf_list - lmax).sum())

    return lsum

def score_pv_single(int_score, term_num, tf_num, keys, vals):
    if int_score == 0:
        pv = 0
    else:
        pv = term_num*np.log10(tf_num) - log10_sum(vals[keys >= int_score])

    return pv

def score_pv_list(int_score, term_num, tf_num, lnf_hash, bins=1000):

    keys = to_array(lnf_hash.keys())
    vals = to_array(lnf_hash.values())

    pv = to_array(map(lambda x: score_pv_single(x, term_num, tf_num, keys,
                                                vals), int_score))

    return pv

def score_pval(score_mat, bins=1000):

    term_num, tf_num = score_mat.shape
    int_mat = (score_mat*bins).astype(np.int64)
    int_score = np.sum(int_mat, axis=0)

    lnf_hash = compute_lnf_hash(int_mat, bins)

    pv = score_pv_list(int_score, term_num, tf_num, lnf_hash, bins)

    return pv

def cond_pval(score, score_mat, bins=1000):
    """
    Compute score by score p_val, conditioned on the previous results.
    """

    working_score = np.copy(score)
    working_score_mat = np.copy(score_mat)

    cond_p = np.zeros_like(score)

    total_nz = np.sum(score != 0)

    for i in range(total_nz):

        logger.info('Working on score %d of %d.' % (i+1, total_nz))
        pv = score_pval(working_score_mat)

        winning_tf = np.argmax(working_score)

        my_pv = np.max(pv)
        cond_p[winning_tf] = my_pv

        working_score[winning_tf] = 0
        working_score_mat[:,winning_tf] = 0

    return cond_p

##################################################################


class Run_task():
    '''
    This task holds information to run the pipeline for a given query and library data.
    We assume Run_task.run() will be called after the initialization procedure.
    '''
    def __init__(
        self, settings,
        reg_dict, tf_dict, tf_sparse_data,
        term_dict, term_sparse_data,
    ):
        self.settings         = settings
        self.reg_dict         = reg_dict
        self.tf_dict          = tf_dict
        self.tf_sparse_data   = tf_sparse_data
        self.term_dict        = term_dict
        self.term_sparse_data = term_sparse_data
        self.run_status = set()

    def _generate_regions(self):
        # Make tmp_dir
        self.settings.tmp_dir_make()

        # Generate regions
        logger.info('Generating regions.')
        gen_regions(
            self.settings.query_path(),
            self.settings.unique_tfbs_path(),
            self.settings.unique_region_file(),
            self.settings.get()
        )
        gen_regions(
            self.settings.query_path(),
            self.settings.merge_tfbs_path(),
            self.settings.merge_region_file(),
            self.settings.get()
        )
        self.run_status.add('_generate_regions')

    def _generate_terms(self):
        assert('_generate_regions' in self.run_status)
        logger.info('Generating terms.')
        self.run_term_names = gen_terms(
            self.settings.merge_region_file(),
            self.settings.get()
        )
        logger.info(('Top terms: {}'.format(', '.join(self.run_term_names))))
        self.run_status.add('_generate_terms')

    def _construct_run_term_tf_mat(self):
        assert('_generate_terms' in self.run_status)
        logger.info('Hashing regions.')
        run_regions = hash_regions(self.settings.unique_region_file(), self.reg_dict)
        self.run_terms = [self.term_dict[term] for term in self.run_term_names]

        logger.info('Subsetting.')
        self.run_term_tf, self.n_vec = run_matrix(
            run_regions,
            self.run_terms,
            self.tf_sparse_data,
            self.term_sparse_data
        )
        if not self.settings.get('leaveTrace'):
            self.settings.tmp_dir_cleanup()
        self.run_status.add('_construct_run_term_tf_mat')

    #@profile
    def _compute_pvals(self):
        assert('_construct_run_term_tf_mat' in self.run_status)
        logger.info('Computing hyper p-values.')
        self.hyp_p_val_matrix = hyper_mat(self.run_term_tf, self.n_vec)

        logger.info('Computing binomial p-values.')
        prob_vec = p_vec(
            self.run_terms,
            self.settings.term_sizes(),
            self.term_dict,
            genome_size=self.settings.chrom_sizes()
        )
        self.bin_p_val_matrix = binom_mat(
            self.run_term_tf,
            self.n_vec,
            prob_vec
        )
        self.run_status.add('_compute_pvals')

    def _generate_results(self):
        assert('_compute_pvals' in self.run_status)
        logger.info('Computing mask')
        mask = compute_filter(
            self.settings.get(),
            is_p_value_in_log_scale = True,
            hyp_p = self.hyp_p_val_matrix,
            bin_p = self.bin_p_val_matrix
        )

        logger.info('Computing scores')
        score_mat, score = compute_score(
            self.hyp_p_val_matrix,
            mask = mask,
            is_p_value_in_log_scale = True
        )

        logger.info('Computing pvalues for score')
        pv = cond_pval(
            score,
            score_mat,
            bins=self.settings.get('pvBins')
        )

        tf_dict_inv = {v:k for k, v in self.tf_dict.items()}
        tf_list = [tf_dict_inv[i] for i in range(len(tf_dict_inv))]

        logger.info('Tabulating the results')
        results = generate_results_df(
            score, tf_list, pv,
            self.settings.get()
        )
        term_contribution = generate_term_contribution_df(score_mat, tf_list, self.run_term_names)

        self.run_status.add('_generate_results')
        return results, term_contribution

    #@profile
    def run(self):
        self._generate_regions()
        self._generate_terms()
        self._construct_run_term_tf_mat()
        self._compute_pvals()
        return self._generate_results()


class Run_task_single(Run_task):
    '''
    Single run mode. (as oppose to server run mode).
    With this setup, the program first reads the relevant data (ontology and TFBS) and
    prepare for a run.
    '''
    def __init__(self, config_run):
        settings = config_run

        logger.info('Importing data.')
        reg_dict, tf_dict, tf_sparse_data = load_assembly(settings.assembly_data())
        term_dict, term_sparse_data = load_ontology(settings.ontology_data())

        Run_task.__init__(
            self, settings,
            reg_dict, tf_dict, tf_sparse_data,
            term_dict, term_sparse_data,
        )

class Run_task_server(Run_task):
    def __init__(self, config_run, reg_dict, tf_dict, tf_sparse_data, term_dict, term_sparse_data):
        Run_task.__init__(
            self, config_run,
            reg_dict, tf_dict, tf_sparse_data,
            term_dict, term_sparse_data,
        )

################
# Simple utils
################

def to_array(iter):
    """
    Convert iterator type object to np.array
    """

    return(np.array(list(iter)))
