#!/bin/bash

# SRCNAME=$(readlink -f $0)
# SRCDIR=$(dirname ${SRCNAME})

# cd ${SRCDIR}

assembly=$1 # hg19 hg38 mm9 mm10

data_url="http://bejerano.stanford.edu/whichtf/reference_data/current/${assembly}.tar.gz"

if [ -d "${assembly}" ] ; then
    read -p "This will overwrite any existing data in $(pwd)/${assembly}. Do you want to continue (y/n)? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ ${REPLY} =~ ^[Yy]$ ]] ; then
        rm -r "${assembly}"
    fi
fi

if [ -s "${assembly}.tar.gz" ] ; then rm "${assembly}.tar.gz" ; fi
wget "${data_url}"
tar -xzvf ${assembly}.tar.gz
