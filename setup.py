try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'WhichTF identifies key transcription factors in open chromatin.',
    'author': 'Ethan S Dyer & Yosuke Tanigawa',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'ethansdyer@gmail.com.',
    'version': '0.2',
    'install_requires': ['nose', 'numpy', 'scipy', 'pandas', 'rpy2==2.9.3'],
    'packages': ['whichtf'],
    'scripts': ['bin/WhichTF'],
    'name': 'whichtf'
}

setup(**config)
